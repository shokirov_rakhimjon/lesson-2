//  Task 1
function is_Devisible_3_5 (n) {
    return n%3 === 0 && n%5 === 0
}


//  Task 2
function is_odd_even (n) {
    return n % 2 === 0 ? 'even': 'odd'
} 
var is_odd_even_2 = (n) => n % 2 === 0 ? 'even': 'odd'


//  Task3
// function sort_array (arr) {
//     if (arr.length < 2) {
//         return arr
//     }
//     pivot = arr.pop()
//     var smaller = []
//     var bigger = []
//     for (let i = 0; i<arr.length; i++){
//         if (arr[i]<=pivot){
//             smaller.push(arr[i])
//         }
//         else {
//             bigger.push(arr[i])
//         }
//     }
//     return sort_array(smaller) + [pivot] + sort_array(bigger) 
// }

//  Task 3
function sort_list (arr) {
    var new_lt = []
    const a = arr.length
    for (let i=0; i<a; i++){
        var minimum_index = get_min_index(arr)
        new_lt.push(arr[minimum_index])
        delete arr[minimum_index]
    }
    return new_lt
}
function get_min_index (values) {
    var min_index_arr = 0
    for (let i=1; i<values.length; i++){
        if (values[i] < values[min_index_arr]){
            min_index_arr = i
        }
    }
    return min_index_arr
}


//  Task 4
var array = [
    {test: ['a', 'b', 'c', 'd']},
    {test: ['a', 'b', 'c']},
    {test: ['a', 'd']},
    {test: ['a', 'b', 'k', 'e', 'e']},
    ]
function unique_set(arr) {
    const mySet1 = new Set()
    for (let i=0; i<array.length; i++){
        for (let j=0; j<array[i]['test'].length; j++){
            mySet1.add(array[i]['test'][j])
        }   
    }    
    return mySet1
}


//  Task 5
function isObjectsame (obj1, obj2) {
    var cnt = 0
    if (Object.keys(obj1).length == Object.keys(obj2).length){
        for (key in obj2) {
            if (key===key && obj1[key] === obj2[key]) {
                cnt++
            }
        }
        return cnt === Object.keys(obj1).length
    return false
    }
}




// console.log(is_Devisible_3_5(15));               output: =>true
// console.log(is_odd_even(5));                     output: =>odd 
// console.log(is_odd_even_2(6));                   output: =>even 
// console.log(sort_list([82, 65, 45, 0]));         output: => [ 0, 45, 65, 82 ]
// console.log(unique_set(array));                  output: => [‘a’, ‘b’, ‘c’, ‘d’, ‘k’, ‘e’]

// var a = {  hair: 'long', beard: true }
// var b = { hair: 'long', beard: true }
// console.log(isObjectsame(a, b))                  output: => true 